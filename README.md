# Just a clock

This is an opensource app for Android.

- Made with Flutter
- Simple, fast and with a modern UI

## Features
- Add, remove and edit your alarms
- Simple countdown timer and timer

## Images
<p align="center">
  <img src="App_pres.png" alt="flutter alarm clock app" title="Screenshot">
</p>

## Get it
You can keep it updated via the F-Droid client here :

<a href="https://apt.izzysoft.de/fdroid/index/apk/com.example.justaclock" target="_blank"><img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png" alt="Get it on FDroid" style="height: 24px !important;width: 48px !important;" ></a>

## Contribution
Feel free to contribute!
