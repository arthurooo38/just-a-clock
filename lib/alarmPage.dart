import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:libreclock/notification_service.dart';
import 'package:libreclock/styles.dart';
import 'dart:async';
import 'package:libreclock/prefs.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


class AlarmPage extends StatefulWidget {
  const AlarmPage({Key? key}) : super(key: key);

  @override
  _AlarmPageState createState() => _AlarmPageState();
}

class _AlarmPageState extends State<AlarmPage> {

  Timer? timer;
  final _formKey0 = GlobalKey<FormState>();
  String text = "";
  int _min = 00;
  int _hour = 00;
  bool _pressing = false;

  addAlarm() {
    showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setStateAlert) {
              return AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                backgroundColor: Theme.of(context).colorScheme.surface,
                content: Form(
                  key: _formKey0,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 16),
                        height: 200,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Center(
                              child: Container(
                                width: 100,
                                child: ListWheelScrollView.useDelegate(
                                    itemExtent: 50,
                                    physics: BouncingScrollPhysics(),
                                    onSelectedItemChanged: (index) {
                                      setStateAlert(() {
                                        _hour = index;
                                      });
                                    },
                                    childDelegate: ListWheelChildBuilderDelegate(builder : (context, index) {
                                      if(index<0 || index >23) {return null;}
                                      return Container(
                                        margin: EdgeInsets.symmetric(horizontal: 4),
                                        child: GestureDetector(
                                          onTap: () {
                                            _hour = index;
                                          },
                                          child: Center(child: Text((index).toString().padLeft(2, "0"),
                                              style: TextStyle(color: index == _hour? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primary.withOpacity(.4),
                                                  fontSize: 20,
                                                  fontWeight: index == _hour? FontWeight.w700 :  FontWeight.w400
                                              )
                                            )
                                          ),
                                        ),
                                      );
                                    }
                                    )
                                ),
                              ),
                            ),
                            Text(
                              "  :  ", style: title,
                            ),
                            Center(
                              child: Container(
                                width: 100,
                                child: ListWheelScrollView.useDelegate(
                                    itemExtent: 50,
                                    physics: BouncingScrollPhysics(),
                                    onSelectedItemChanged: (index) {
                                      setStateAlert(() {
                                        _min = index;
                                      });
                                    },
                                    childDelegate: ListWheelChildBuilderDelegate(builder : (context, index) {
                                      if(index<0 || index >59) {return null;}
                                      return Container(
                                        margin: EdgeInsets.symmetric(horizontal: 4),
                                        child: GestureDetector(
                                          onTap: () {
                                            _min = index;
                                          },
                                          child: Center(child: Text((index).toString().padLeft(2, "0"),
                                              style: TextStyle(color: index == _min? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primary.withOpacity(.4),
                                                fontSize: 20,
                                                fontWeight: index == _min? FontWeight.w700 :  FontWeight.w400
                                              ),
                                            )
                                          ),
                                        ),
                                      );
                                    }
                                    )
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(bottom: 32),
                        child: TextFormField(
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Theme.of(context).colorScheme.onSurface.withOpacity(.06),
                              hintText: AppLocalizations.of(context)!.enterNameAlarm
                          ),
                          textCapitalization: TextCapitalization.sentences,
                          onChanged: (t) {
                            text = t;
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                actions: [
                  TextButton(
                      style: buttonStyle,
                      onPressed: () {Navigator.pop(context);},
                      child: Text(AppLocalizations.of(context)!.cancel, style: buttonText.copyWith(color: Theme.of(context).colorScheme.secondary))
                  ),
                  TextButton(
                      style: buttonStyle,
                      onPressed: () {
                        if(_formKey0.currentState!.validate()) {
                          setState(() {
                            alarmsTime.add(_hour.toString().padLeft(2, "0") + ":" + _min.toString().padLeft(2, "0")); alarmsActive.add("true"); alarmsName.add(text);
                            NotifService().showAlarm(decomposeTime(alarmsTime[alarmsTime.length-1])[0], decomposeTime(alarmsTime[alarmsTime.length-1])[1], alarmsName[alarmsTime.length-1], alarmsTime.length-1);
                            Local().save("alarmsTime", alarmsTime); Local().save("alarmsActive", alarmsActive); Local().save("alarmsName", alarmsName);
                          });
                          Navigator.pop(context);
                        }
                      },
                      child: Text(AppLocalizations.of(context)!.ok, style: buttonText.copyWith(color: Theme.of(context).colorScheme.primary))
                  )
                ],
              );
            }
          );
        }
    );
  }

  modifAlarm(int index) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            height: 60,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      for(int i = 0; i < alarmsTime.length; i++) {
                        NotifService().cancel(i);
                      }
                      alarmsTime.removeAt(index); alarmsActive.removeAt(index); alarmsName.removeAt(index);
                      Local().save("alarmsTime", alarmsTime); Local().save("alarmsActive", alarmsActive); Local().save("alarmsName", alarmsName);
                      NotifService().cancel(index);
                    });
                    for(int i = 0; i < alarmsTime.length; i++) {
                      if(alarmsActive[i] == "true") {
                        NotifService().showAlarm(decomposeTime(alarmsTime[i])[0], decomposeTime(alarmsTime[i])[1], alarmsName[i], i);
                      }
                    }
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 16),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 32),
                          child: Icon(Icons.delete_forever, color: Theme.of(context).colorScheme.secondary),
                        ),
                        Text(AppLocalizations.of(context)!.delete, style: buttonText.copyWith(color: Theme.of(context).colorScheme.secondary)),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        }
    );
  }


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              itemCount: alarmsTime.length+1,
              itemBuilder: ((context, int index) {
                return index!= alarmsTime.length? Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(20),
                    onTap: () {modifAlarm(index);},
                    child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Theme.of(context).colorScheme.surface
                        ),
                      child: AnimatedOpacity(
                        duration: Duration(milliseconds: 150),
                        opacity: alarmsActive[index] == "true"? 1:0.4,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(alarmsTime[index], style: hourText.copyWith(color: alarmsActive[index] == "true"? Theme.of(context).colorScheme.onSurface : Theme.of(context).colorScheme.onSurface)),
                                Text(alarmsName[index], style: buttonText.copyWith(color: Theme.of(context).colorScheme.onSurface.withOpacity(.6)))
                              ],
                            ),
                            Switch(
                              activeColor: Theme.of(context).colorScheme.secondary,
                                value: alarmsActive[index] == "true",
                                onChanged: (bool value) {
                                  setState(() {
                                    value? alarmsActive[index] = "true" : alarmsActive[index] = "false";
                                  });
                                  Local().save("alarmsActive", alarmsActive);
                                  value? NotifService().showAlarm(decomposeTime(alarmsTime[index])[0], decomposeTime(alarmsTime[index])[1], alarmsName[index], index): NotifService().cancel(index);
                                })
                          ],
                        ),
                      ),
                    ),
                  ),
                ) :
                Center(
                  child: SizedBox(
                    height: 80, width: 110,
                    child: GestureDetector(
                      onTapDown: (TapDownDetails _) {
                        setState(() {
                          _pressing = true;
                        });
                      },
                      onTapUp: (TapUpDetails _) {
                        setState(() {
                          _pressing = false;
                        });
                      },
                      onTapCancel: () {
                        setState(() {
                          _pressing = false;
                        });
                      },
                        onTap: (){
                          setState(() {
                            text = "";
                            _min = 0; _hour = 0;
                            addAlarm();
                          });
                        },
                      child: AnimatedContainer(
                        margin: _pressing? EdgeInsets.all(5) : EdgeInsets.all(0),
                        duration: Duration(milliseconds: 200),
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.primary,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Icon(Icons.add_rounded, color: Theme.of(context).colorScheme.onPrimary, size: 36,)
                      ),
                    ),
                  ),
                );
              }),
            )
        ),

      ],
    );
  }
}
