import 'package:flutter/material.dart';
import 'package:libreclock/styles.dart';
import 'package:libreclock/prefs.dart';


class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                  format24 = true;
                });
              },
              child: AnimatedContainer(
                padding: EdgeInsets.symmetric(horizontal: 32, vertical: 32),
                margin: EdgeInsets.symmetric(horizontal: 2, vertical: 16),
                duration: Duration(milliseconds: 200),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.horizontal(right: Radius.circular(4), left: Radius.circular(16)),
                  color: format24? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.surface
                ),
                child: Text("24h format", style: TextStyle(fontSize: 18, color: format24? Theme.of(context).colorScheme.onPrimary : Theme.of(context).colorScheme.onSurface)),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  format24 = false;
                });
              },
              child: AnimatedContainer(
                padding: EdgeInsets.symmetric(horizontal: 32, vertical: 32),
                margin: EdgeInsets.symmetric(horizontal: 2, vertical: 16),
                duration: Duration(milliseconds: 200),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.horizontal(right: Radius.circular(16), left: Radius.circular(4)),
                    color: !format24? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.surface
                ),
                child: Text("12h format", style: TextStyle(fontSize: 18, color: !format24? Theme.of(context).colorScheme.onPrimary : Theme.of(context).colorScheme.onSurface))
              ),
            ),
          ],
        ),
        TextButton(
          onPressed: () {
            showAboutDialog(
              context: context,
              applicationName: "Just a clock",
              applicationVersion: version
            );
          },
          child: Text("About"),
          style: buttonStyle
        )
      ],
    );
  }
}
