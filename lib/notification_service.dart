import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';

void callbackPlay() {
  FlutterRingtonePlayer.play(android: AndroidSounds.alarm, ios: IosSounds.glass, looping: false, asAlarm: true, );
}
void callbackStop() {
  FlutterRingtonePlayer.stop();
}

class NotifService {

  init() async {
    AwesomeNotifications().initialize(
        null,
        [
          NotificationChannel(
              channelKey: 'timer',
              channelName: 'End of timer',
              channelDescription: 'Notification channel for the end of the countdown timer',
              defaultColor: Colors.lightBlue[200],
              ledColor: Colors.white,
              enableVibration: true,
              importance: NotificationImportance.Max,
              playSound: false,
          ),
          NotificationChannel(
            channelKey: 'alarm',
            channelName: 'This is an alarm',
            channelDescription: 'Notification channel for an alarm ring',
            defaultColor: Colors.lightBlue[200],
            ledColor: Colors.white,
            enableVibration: true,
            importance: NotificationImportance.Max,
            playSound: false,
          )
        ]
    );

  }

  show(int mins, int secs) async {
    String localTimeZone = await AwesomeNotifications().getLocalTimeZoneIdentifier();

    AwesomeNotifications().createNotification(
        content: NotificationContent(
          id: 100,
          channelKey: 'timer',
          title: 'Time is up!',
        ),
      schedule: NotificationInterval(interval: mins*60+secs, timeZone: localTimeZone),
    );
    await AndroidAlarmManager.oneShot(Duration(minutes: mins, seconds: secs), 100, callbackPlay, exact: true, alarmClock: true, wakeup: true, allowWhileIdle: true, rescheduleOnReboot: false);
  }

  showAlarm(int minutes, int hours, String name, index) async {
    String localTimeZone = await AwesomeNotifications().getLocalTimeZoneIdentifier();
    AwesomeNotifications().createNotification(
        content: NotificationContent(
            id: index,
            channelKey: 'alarm',
            title: name==""? "Alarm": name,

        ),
        schedule: NotificationCalendar(timeZone: localTimeZone, hour: hours, minute: minutes, second: 0,millisecond: 0, repeats: true),
        actionButtons: [NotificationActionButton(key: 'repeat', label: "Repeat", autoDismissable: true)]
    );
    await AndroidAlarmManager.periodic(Duration(days: 1), index, callbackPlay, exact: true, startAt: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, hours, minutes, 0), wakeup: true, allowWhileIdle: true, rescheduleOnReboot: true);
  }

  repeatAlarm() async {
    String localTimeZone = await AwesomeNotifications().getLocalTimeZoneIdentifier();
    AwesomeNotifications().createNotification(
        content: NotificationContent(
          id: 101,
          channelKey: 'alarm',
          title: "Repeated alarm",

        ),
        schedule: NotificationInterval(interval: 300, timeZone: localTimeZone),
        actionButtons: [NotificationActionButton(key: 'repeat', label: "Repeat", autoDismissable: true)]
    );
  }

  stopSound() async {
    for(int i = 0; i<101; i++) {
      await AndroidAlarmManager.oneShotAt(DateTime.now(), i, callbackStop, exact: true, allowWhileIdle: true, alarmClock: true);
    }
  }

  cancel(int index) {
    AwesomeNotifications().cancel(index);
    AndroidAlarmManager.cancel(index);
  }
  cancelAll() {
    AwesomeNotifications().cancelAll();
    for(int i =0; i<101; i++) {
      AndroidAlarmManager.cancel(i);
    }
  }

}