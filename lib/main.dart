import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:libreclock/countdownPage.dart';
import 'package:libreclock/timerPage.dart';
import 'package:libreclock/styles.dart';
import 'package:libreclock/notification_service.dart';
import 'package:libreclock/alarmPage.dart';
import 'package:libreclock/prefs.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AndroidAlarmManager.initialize();
  alarmsActive = await Local().get("alarmsActive")?? []; alarmsTime = await Local().get("alarmsTime")?? [];
  alarmsName = await Local().get("alarmsName")?? [];
  await NotifService().init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(localizationsDelegates: [
      AppLocalizations.delegate,
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
    ],
      supportedLocales: [
        Locale('fr', ''),
        Locale('en', ''),
      ],
      themeMode: ThemeMode.system,
      theme: lightTheme,
      darkTheme: darkTheme,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  int _currentIndex = 0;

  @override
  void initState() {
    AwesomeNotifications().dismissedStream.listen((receivedNotification) {
      NotifService().stopSound();
    });
    AwesomeNotifications().actionStream.listen((receivedNotification) {
      NotifService().stopSound();
      if (receivedNotification.buttonKeyPressed == 'repeat') {
        NotifService().repeatAlarm();
      }
    }
  );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    void _updateIndex(int index) {
      setState(() {
        _currentIndex = index;
      });
    }


    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Theme.of(context).colorScheme.background,
      body: SafeArea(
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 26),
              width: MediaQuery.of(context).size.width,
              child: Text(
                "Just a Clock", style: title.copyWith(color: Theme.of(context).colorScheme.onBackground.withOpacity(.6)), textAlign: TextAlign.left,
              ),
            ),
            Expanded(
              child: IndexedStack(
                index: _currentIndex,
                children: [
                  AnimatedOpacity(
                    opacity: _currentIndex==0? 1 : 0,
                    curve: Curves.easeInOut,
                    duration: Duration(milliseconds: 500),
                    child: AlarmPage()
                  ),
                  AnimatedOpacity(
                      opacity: _currentIndex==1? 1 : 0,
                      curve: Curves.easeInOut,
                      duration: Duration(milliseconds: 500),
                      child: CountDown()
                  ),
                  AnimatedOpacity(
                      opacity: _currentIndex==2? 1 : 0,
                      curve: Curves.easeInOut,
                      duration: Duration(milliseconds: 500),
                      child: TimerPage()
                  ),
                  //SettingsPage()
                ],
              ),
            ),
            Divider(height: 1,)
          ],
        ),
      ),
      bottomNavigationBar: Container(
        height: 68,
        child: BottomNavigationBar(
          onTap: _updateIndex,
          backgroundColor: Theme.of(context).colorScheme.surface,
          elevation: 0,
          selectedItemColor: Theme.of(context).colorScheme.primary,
          currentIndex: _currentIndex,
          showUnselectedLabels: false, showSelectedLabels: false,
          items: [
            BottomNavigationBarItem(
                icon: SvgPicture.asset("img/alarm.svg", height: 28, width: 28, color: _currentIndex==0? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primaryVariant,), label: AppLocalizations.of(context)!.alarm),
            BottomNavigationBarItem(
                icon: SvgPicture.asset("img/hourglass.svg", height: 28, width: 28, color: _currentIndex==1? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primaryVariant,), label: AppLocalizations.of(context)!.countdown),
            BottomNavigationBarItem(
                icon: SvgPicture.asset("img/timer.svg", height: 28, width: 28, color: _currentIndex==2? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primaryVariant,), label: AppLocalizations.of(context)!.timer),
          ],
        ),
      ),
    );
  }

}