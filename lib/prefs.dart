import 'package:shared_preferences/shared_preferences.dart';


List<String> alarmsTime = [

];

List<String> alarmsActive = [

];

List<String> alarmsName = [

];

bool format24 = true;

class Local {

  get(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(key);
  }
  save(String key, List<String> toSave) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList(key, toSave);
  }

  getTimer(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }
  saveTimer(String key, String toSave) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, toSave);
  }

  getBool(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }
  saveBool(String key, bool toSave) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, toSave);
  }

  getInt(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key);
  }
  saveInt(String key, int toSave) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, toSave);
  }
}