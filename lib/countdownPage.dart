import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:libreclock/styles.dart';
import 'dart:async';
import 'package:libreclock/notification_service.dart';
import 'package:libreclock/prefs.dart';


class CountDown extends StatefulWidget {
  const CountDown({Key? key}) : super(key: key);

  @override
  _CountDownState createState() => _CountDownState();
}

class _CountDownState extends State<CountDown> {

  int mins = 1;
  int secs = 0;
  bool timerLaunched = false;
  DateTime _timeNow = DateTime.now();
  DateTime? timeRing;
  bool _pressing = false;


  @override
  void initState() {
    getVariables();
    Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _timeNow = DateTime.now();
        if(timeRing != null && timeRing!.isBefore(_timeNow)) {timeRing = null; timerLaunched = false; Local().saveBool("timerLaunched", timerLaunched);}
      });
    });
    super.initState();
  }


  getVariables() async {
    String? _timeRing = await Local().getTimer("_timeRing")?? null;
    _timeRing!=null? timeRing = DateTime.parse(_timeRing) : timeRing = null;

    timerLaunched = await Local().getBool("timerLaunched")?? false;
    mins = await Local().getInt("mins")?? 1;
    secs = await Local().getInt("secs")?? 0;
  }

  @override
  Widget build(BuildContext context) {

    stopTime() {
      NotifService().cancel(100);
      setState(() {
        timeRing = null;
        Local().saveTimer("_timeRing", timeRing.toString());
        timerLaunched = false;
      });
    }

    launchTime() async {
      timeRing = DateTime.now().add(Duration(minutes: mins, seconds: secs));
      Local().saveTimer("_timeRing", timeRing.toString());
      NotifService().show(mins, secs);

      setState(() {
        timerLaunched = true;
      });
    }


    return Column(
      children: [
        Expanded(
          child: Center(
            child: AnimatedContainer(
              duration: Duration(milliseconds: 200),
              decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.surface,
                borderRadius: timerLaunched? BorderRadius.circular(200) : BorderRadius.circular(20)
              ),
              width: MediaQuery.of(context).size.width-64, height: MediaQuery.of(context).size.width-64,
              child: AnimatedSwitcher(
                duration: Duration(milliseconds: 200),
                child: timeRing != null && timeRing!.isAfter(_timeNow)?
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.width-64,
                      child: CircularProgressIndicator(
                        backgroundColor: Theme.of(context).colorScheme.surface,
                        valueColor: AlwaysStoppedAnimation<Color> (Theme.of(context).colorScheme.secondary),
                        strokeWidth: 8,
                        value: timerLaunched
                            ? 1 - timeRing!.difference(_timeNow).inSeconds / (mins*60 + secs)
                            : 0,
                      ),
                    ),
                    Container(
                      child: Text(timeRing!.difference(_timeNow).inMinutes.toString() + ":" + timeRing!.subtract(Duration(minutes: timeRing!.difference(_timeNow).inMinutes)).difference(_timeNow).inSeconds.toString().padLeft(2, "0"),
                            style: hugeText.copyWith(color: Theme.of(context).colorScheme.onBackground),)
                    ),
                  ],
                ):
                Center(
                  child: Container(
                    height: 240,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Theme.of(context).colorScheme.surface
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 32),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Container(
                            width: 100,
                            child: ListWheelScrollView.useDelegate(
                              itemExtent: 50,
                              physics: BouncingScrollPhysics(),
                              onSelectedItemChanged: (index) {
                                setState(() {
                                  mins = index+1;
                                });
                                Local().saveInt("mins", mins);
                              },
                              childDelegate: ListWheelChildBuilderDelegate(builder : (context, index) {
                                if(index<0 || index >58) {return null;}
                                return Container(
                                  margin: EdgeInsets.symmetric(horizontal: 4),
                                  child: GestureDetector(
                                    onTap: () {
                                      mins = index+1;
                                      Local().saveInt("mins", mins);
                                    },
                                    child: Center(child: Text((index + 1).toString(),
                                        style: TextStyle(color: index+1 == mins? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primary.withOpacity(.4),
                                            fontSize: 20,
                                            fontWeight: index+1 == mins? FontWeight.w700 :  FontWeight.w400
                                        )
                                      )
                                    ),
                                  ),
                                );
                              }
                            )
                            ),
                          ),
                        ),
                        Text(
                          "  :  ", style: title,
                        ),
                        Center(
                          child: Container(
                            width: 100,
                            child: ListWheelScrollView.useDelegate(
                                itemExtent: 50,
                                physics: BouncingScrollPhysics(),
                                onSelectedItemChanged: (index) {
                                  setState(() {
                                    secs = index;
                                  });
                                  Local().saveInt("secs", secs);
                                },
                                childDelegate: ListWheelChildBuilderDelegate(builder : (context, index) {
                                  if(index<0 || index >59) {return null;}
                                  return Container(
                                    margin: EdgeInsets.symmetric(horizontal: 4),
                                    child: GestureDetector(
                                      onTap: () {
                                        secs = index;
                                        Local().saveInt("secs", secs);
                                      },
                                      child: Center(child: Text((index).toString().padLeft(2, "0"),
                                          style: TextStyle(color: index == secs? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primary.withOpacity(.4),
                                              fontSize: 20,
                                              fontWeight: index == secs? FontWeight.w700 :  FontWeight.w400
                                          )
                                        )
                                      ),
                                    ),
                                  );
                                }
                                )
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        GestureDetector(
          onTapDown: (TapDownDetails _) {
            setState(() {
              _pressing = true;
            });
          },
          onTapUp: (TapUpDetails _) {
            setState(() {
              _pressing = false;
            });
          },
          onTapCancel: () {
            setState(() {
              _pressing = false;
            });
          },
          onTap: () {
            if(timerLaunched) {
              setState(() {
                stopTime();
                mins = 1;
                secs = 0;
              });
            } else {
              setState(() {
                launchTime();
              });
            }
            Local().saveBool("timerLaunched", timerLaunched);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 32),
            child: SizedBox(
              height: 80, width: 110,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  AnimatedContainer(
                    margin: _pressing? EdgeInsets.all(5) : EdgeInsets.all(0),
                    decoration: BoxDecoration(
                      color: timerLaunched? Theme.of(context).colorScheme.secondary : Theme.of(context).colorScheme.primary,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    duration: Duration(milliseconds: 200),
                  ),
                  timerLaunched? Icon(Icons.stop_rounded, color: Theme.of(context).colorScheme.onSecondary, size: 48) :
                    Icon(Icons.play_arrow_rounded, color: Theme.of(context).colorScheme.onPrimary, size: 48)
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

}


