import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:core';

///Styles
final TextStyle hugeText = TextStyle(fontSize: 90, fontWeight: FontWeight.w800, fontFamily: "Nunito");
final TextStyle title = TextStyle(fontSize: 26, fontFamily: "Nunito", letterSpacing: .6, fontWeight: FontWeight.w800);
final TextStyle buttonText = TextStyle(fontSize: 13, fontWeight: FontWeight.w700, fontFamily: "Nunito");
final TextStyle hourText = TextStyle(fontSize: 28, fontWeight: FontWeight.w800, fontFamily: "Nunito");

final ButtonStyle buttonStyle = TextButton.styleFrom(shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(16)), elevation: 0,
    padding: EdgeInsets.symmetric(horizontal: 32, vertical: 16));

InputDecoration textFieldDeco(Color color) {
  return InputDecoration(
    filled: true,
    fillColor: color
  );
}


///Components


ThemeData lightTheme = ThemeData(
  fontFamily: "Nunito", primaryColor: Colors.white,
  colorScheme: ColorScheme(
    brightness: Brightness.light,
    background: Color(0xffF7F9FC),
    onBackground: Color(0xff000000),

    primary: Color(0xff0066FF),
    onPrimary: Color(0xffFFFFFF).withOpacity(.9),
    primaryVariant: Color(0xff000000).withOpacity(.24),

    secondary: Color(0xFFF24952),
    onSecondary: Color(0xffFFFFFF).withOpacity(.9),
    secondaryVariant: Color(0xFFF24952).withOpacity(.16),

    surface: Color(0xffFFFFFF),
    onSurface: Color(0xff000000),

    error: Color(0xffFF5252),
    onError: Color(0xff212121)
  )
);

ThemeData darkTheme = ThemeData(
  fontFamily: "Nunito", primaryColor: Colors.black,
    colorScheme: ColorScheme(
      brightness: Brightness.dark,
        background: Colors.black,
        onBackground: Color(0xffFFFFFF),

        primary: Color(0xff99C2FF),
        onPrimary: Color(0xff000000).withOpacity(.9),
        primaryVariant: Color(0xffFFFFFF).withOpacity(.24),

        secondary: Color(0xFFF17E84),
        onSecondary: Color(0xff000000).withOpacity(.9),
        secondaryVariant: Color(0xFFF17E84).withOpacity(.16),

        surface: Color(0xff12171F),
        onSurface: Color(0xffFFFFFF),

        error: Color(0xffFF5252),
        onError: Color(0xff212121)
    )
);

///Version
String version = "1.2.3";

List<int> decomposeTime(String time) {
  int hour = 0; int min = 0;
   int _indexOf = time.indexOf(":");
  hour = int.parse(time.substring(0, _indexOf));
  min = int.parse(time.substring(_indexOf+1, time.length));
  return [min, hour];
}