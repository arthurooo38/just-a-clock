import 'package:flutter/material.dart';
import 'package:libreclock/styles.dart';
import 'dart:async';

class TimerPage extends StatefulWidget {
  const TimerPage({Key? key}) : super(key: key);

  @override
  _TimerPageState createState() => _TimerPageState();
}

class _TimerPageState extends State<TimerPage> {

  bool timerLaunched = false;
  bool timerStopped = false;
  int time = 0;
  Timer? timer ;
  bool _pressing = false;


  stopTime() {
    timer!.cancel();
    setState(() {
      time = 0; timerLaunched = false;
    });
  }

  launchTime() async {
    setState(() {
      timerLaunched = true;
    });
    timer = Timer.periodic(Duration(seconds: 1), (_timer) {
      setState(() {
        time =time +1;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(32),
            child: InkResponse(
              radius: 64,
              onTap: () {
                if(timerLaunched) {
                  if (!timerStopped) {
                    setState(() {
                      timer!.cancel();
                      timerStopped = true;
                    });
                  } else {
                    setState(() {
                      launchTime();
                      timerStopped = false;
                    });
                  }
                }
              },
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.width-64,
                      child: Text("${(time) ~/ 60}:" + ((time) % 60).toString().padLeft(2, "0"),
                        style: hugeText.copyWith(color: Theme.of(context).colorScheme.onBackground),
                      )
                  ),
                  AnimatedOpacity(
                    duration: Duration(milliseconds: 150),
                      opacity: timerStopped? .12 : 0,
                      child: Icon(Icons.pause, size: 240, color: Theme.of(context).colorScheme.onBackground))
                ],
              ),
            ),
          ),
        ),
        GestureDetector(
          onTapDown: (TapDownDetails _) {
            setState(() {
              _pressing = true;
            });
          },
          onTapUp: (TapUpDetails _) {
            setState(() {
              _pressing = false;
            });
          },
          onTapCancel: () {
            setState(() {
              _pressing = false;
            });
          },
          onTap: () {
            if(timerLaunched) {
              setState(() {
                stopTime(); timerStopped = false;
              });
            } else {
              setState(() {
                launchTime();
              });
            }
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 32),
            child: SizedBox(
              height: 80, width: 110,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  AnimatedContainer(
                    margin: _pressing? EdgeInsets.all(5) : EdgeInsets.all(0),
                    decoration: BoxDecoration(
                      color: timerLaunched? Theme.of(context).colorScheme.secondary : Theme.of(context).colorScheme.primary,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    duration: Duration(milliseconds: 200),
                  ),
                  timerLaunched? Icon(Icons.stop_rounded, color: Theme.of(context).colorScheme.onSecondary, size: 48) :
                  Icon(Icons.play_arrow_rounded, color: Theme.of(context).colorScheme.onPrimary, size: 48)
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
